#!/usr/bin/python
#
#   See the README for more info
#
#   Requirement:
#       - python 3.x
#       - SPARQLWrapper (pip install sparqlwrapper )
###################################################################################
import glob
import sys
import json
import os
import warnings
import functools
from SPARQLWrapper import SPARQLWrapper, JSON

AVAILABLE_DBS = ["Wheat", "Arabidopsis"]

WHEAT_VARS = {
    "GRAPH_NAME" : "http://latest.knetminer.wheat.ac.uk",
    "URL" : "http://knetminer.rothamsted.ac.uk/wheatknet/genepage?list=",
    "OUTPUT_PATH" : "output_knetminer/output_wheat",
    "TAXONS_NAMES_JSON" : "taxons_names.json",
    "GENES_URI_JSON" : "wheat_genes_uri.json",
    "GENES_CSV" : "knetminer_wheat_genes.csv"
}

ARABIDO_VARS = {
    "GRAPH_NAME" : "http://latest.knetminer.arabidopsis.ac.uk",
    "URL" : "http://knetminer.rothamsted.ac.uk/araknet/genepage?list=",
    "OUTPUT_PATH" : "output_knetminer/output_arabido",
    "TAXONS_NAMES_JSON" : "taxons_names.json",
    "GENES_URI_JSON" : "arabido_genes_uri.json",
    "GENES_CSV" :"knetminer_arabido_genes.csv"
}


def check_python_ver():
    """
    Check out the Python version.
    """
    if sys.version_info[0] < 3:
        print("Please make sure you're using Python 3")
        sys.exit(1)


def import_or_install(package):
    """
    Import the required packages, otherwise it installs them if
    they doesn't exist (after the user's agreement).

    :param package: the package name which shall be imported or installed
    """
    try:
        __import__(package)
    except ImportError:
        # Ask the user if he want to install the missing module
        answer = yes_no("No module named '" + package + "', do you want to install it ? [y/n]")
        # If answer=yes install it
        if answer: pip.main(['install', "--user", package])
        # Else exit the script
        else: sys.exit(1)


def choose_db(available_dbs):
    """
    Ask the user to choose a database from which we want to extract the information

    :param available_dbs: a list of the available databases
    :return: returns the choosen database
    """
    dbs = ""
    for i, v in enumerate(available_dbs):
        # construct an enumerated string along with the databases names
        dbs += str(i+1)+"."+ v +"\n"
    chosen_db = None
    ques = "Please choose a DB (Enter a number): \n%s" %dbs
    answer = input(ques)
    if answer == "1":
        chosen_db = WHEAT_VARS
    elif answer == "2":
        chosen_db = ARABIDO_VARS
    else:
        print("You didn't choose an existing DB !")
        sys.exit(1)
    return chosen_db


def create_output_dir(path_dir):
    """
    Create the output directory if it doesn't exist

    :param path_dir: the full path of the output directory
    """
    try:
        if not os.path.exists(path_dir):
            os.makedirs(path_dir)
    except PermissionError as e:
        print("%s, Please make sure that you have the permission to write in this directory !")
        sys.exit(1)


def yes_no(question):
    """
    Prompt the question to the user and return the answer.

    :param question: the question which will be prompted to the user
    :return: returns a boolean, 'True' if the user answer's 'yes', 'False' otherwise.
    """
    # ask the user
    answer = input(question).lower()
    # keep asking him until he
    if answer=="": yes_no(question)
    # answers by 'y'
    elif answer[0]=="y": return True
    # or something else
    else: return False


def remove_files(path, extension):
    """
    Remove files with specific extension.

    :param path: the full path of the directory containing the files
    :param extension: the desired extension to remove
    """
    for ext in extension:
        files = glob.glob('%s/*.%s' %(path,ext))
        for f in files:
            os.remove(f)


# def write_genes_uri_list(filename, genes_uri_list):
def json_save(filename, data):
    """
    Save the data into a JSON file.

    :param filename: the filename
    :param data: the data which will be stored in the JSON
    """
    with open(filename, 'w') as f:
        json.dump(data, f)


def read_genes_uri_list(filename):
    """
    Read the genes URIs (stored in a JSON file).

    @param filename: the filename
    @return data: the data which will be stored in the JSON
    """
    try:
        with open(filename, 'r') as f:
            genes_uri_list = json.load(f)
        print("READING GENES URI DONE !")
    except IOError:
        print("Please make sure that the file " + filename + " exists !")
        sys.exit(1)
    return genes_uri_list


def json_load(filename):
    """
    Load data from a JSON file.

    :param filename: the filename
    :param data: the data which will be stored in the JSON
    """
    with open(filename, 'r') as fp:
        data = json.load(fp)
    return data


def read_query_file(file, var=False):
    """
    Read the query file.

    :param file: the file which contain the SPARQL query
    :param var: boolean variable, set to
        'False' if the query doesn't contain variable(s) (e.g no %s)
        'True' if the query does contain variable(s) (e.g gene_batch..)
    :return: returns the query, evaluated in case var=True (evaluate '%s')
        Otherwise, it returns a normal string
    """
    with open(file, 'r') as f:
        query = f.read()
    if var :
        return eval("\'''"+query+"\'''")
    else:
        return query


def sparql_to_json(sparql):
    """
    Take the sparql object and turn it to JSON.

    :param sparql: the sparql object
    :return result: returns the result in JSON format
    """
    sparql.setReturnFormat(JSON)
    try:
        sparql_data = sparql.query().convert()
    except Exception as e:
        print("ERROR: "+str(e)+", Please verify that Virtuoso is started successfully!")
        sys.exit(1)
    # Step needed to encode data structure into JSON
    json_str = json.dumps(sparql_data)
    # Turn a JSON-encoded string into a Python data structure
    json_decoded = json.loads(json_str)
    result = json_decoded["results"]["bindings"]
    return result


# if it's not a gene
def add_generic_element_to_dico(ele, attribs, info, gene_dico, endpoint = "KnetMiner"):
    """
    Add any type of elements such as Proteins, Taxon etc (Except genes)
    to the gene dictionary.

    :param ele: the element itself (e.g Protein)
    :param attribs: list of the element's attributs
        (e.g "protein synonyms", "molecular_function" etc)
    :param info: the result of the sparql query which may contain the attributs values
    :param gene_dico: the gene dictionary in which we store the retrieved information
    :param endpoint: the endpoint
    :return result: returns the element id which can be used to add other sub-elements
    """
    # if the element/dictionary (e.g taxons) doesn't exists, create it
    if ele+'s' not in gene_dico:
        gene_dico[ele+'s'] = {}
    # if (e.g taxon or protein...) in info we got from the query
    # taxon or protein ... : are URIs
    if ele in info:
        # get the URI
        ele_uri = info[ele]["value"]
        # Get the ID
        # The ID depends on the endpoint
        # AgroLD doesn't have an ID field so we retrieve it from the URI
        if endpoint == "AgroLD":
            ele_id = ele_uri.rsplit('/', 1)[-1].upper()
        # Most elements of KnetMiner have an ID field but there are some exceptions
        elif endpoint == "KnetMiner":
            # split two times to get rid ogf http...
            ele_id = ele_uri.split("%23")[-1].upper()
            ele_id = ele_id.split('/')[-1].upper()

        # if this element (e.g NCBITAXON_4533) not in taxons
        if ele_id not in gene_dico[ele+'s']:
            # add it
            gene_dico[ele+'s'][ele_id] = {}
            gene_dico[ele+'s'][ele_id][ele+"_uri"] = ele_uri
            # foreach attribut (e.g taxon_name...)
        for attrib in attribs:
            # if the atrribute exists in the query result
            if attrib in info:
                # create an empty list for it if it doasn't exist already
                if attrib not in gene_dico[ele+'s'][ele_id]: #if not gene_dico[ele+'s'][ele_id][attrib]:
                    gene_dico[ele+'s'][ele_id][attrib] = []
                # Get the corresponding value (e.g Oryza brachyantha)
                attrib_val = info[attrib]["value"]
                # if this value isn't already exists
                if attrib_val not in gene_dico[ele+'s'][ele_id][attrib]:
                    # Add it
                    gene_dico[ele+'s'][ele_id][attrib].append(attrib_val)
        return ele_id


# If it's a gene we add only the attributes
def add_gene_element_to_dico(attribs, info, gene_dico):
    """
    Add gene to the gene dictionary.

    :param attribs: list of the gene's attributs
        (e.g "gene name", "start position" etc)
    :param info: the result of the sparql query which may contain the attributs values
    :param gene_dico: the gene dictionary in which we store the retrieved information
    """
    # foreach attribut (e.g start_pos...)
    for attrib in attribs:
        # if the atrribute exists in the query result
        if attrib in info:
            # create an empty list for it if it doasn't exist already
            if attrib not in gene_dico:
                gene_dico[attrib] = []
            # Get the corresponding value (e.g 257198)
            attrib_val = info[attrib]["value"]
            # if this value isn't already exists
            if attrib_val not in gene_dico[attrib]:
                # Add it
                gene_dico[attrib].append(attrib_val)


def create_gene_batches(genes_uri_list, batch_size):
    """
    Create batch of gene URIs (see Example), this batches will be used to create
    the SPARQL query.

    :param genes_uri_list: list of all genes
    :param batch_size: determine the number of genes in each batch
    :returns: a list of all genes URIs grouped in batches

    Example:
    >>> BATCH_SIZE = 2
    >>> genes_uri_list = ['gene_1_uri', 'gene_2_uri', 'gene_3_uri', 'gene_4_uri']
    >>> genes_uri_batches = create_gene_batches(genes_uri_list, BATCH_SIZE)
    >>> genes_uri_batches
    ['<gene_1_uri> <gene_2_uri>', '<gene_3_uri> <gene_4_uri>']
    """
    genes_uri_batch_list = []
    start = 0
    end = batch_size
    while end < len(genes_uri_list) + batch_size:
        genes_uri_batch = "<" + ("> <".join(genes_uri_list[start:end])) + ">"
        genes_uri_batch_list.append(genes_uri_batch)
        start += batch_size
        end += batch_size
    return genes_uri_batch_list


def get_nbr_of_genes(endpoint, graph_name, query):
    """
    Count the number of genes in the graph (this will be used in pagination).

    :param endpoint: the endpoint
    :param query: the query retrieved from the .rq file
    :returns: the number of genes
    """
    sparql = SPARQLWrapper(endpoint, query)
    sparql.setQuery(query %graph_name)
    nbr_of_genes = sparql_to_json(sparql)
    nbr_of_genes_value = nbr_of_genes[0]["nbr_of_genes"]["value"]
    return int(nbr_of_genes_value)


def get_all_genes_uri(endpoint, graph_name, query, pagesize, the_offset):
    """
    Query the DB and return genes URI in json format.

    :param endpoint: the endpoint
    :param query: the query retrieved from the .rq file
    :param pagesize: specifies the size of a page of output.
        The value of 'pagesize' is the number of output lines to be produced
        used in the query (LIMIT pagesize).
    :param the_offset: controls where the solutions start from in the overall
        sequence of solutions; 'LIMIT' and 'OFFSET' (combined with 'ORDER BY')
        are used in the pagination
    :returns: the genes URIs
    """
    sparql = SPARQLWrapper(endpoint)
    sparql.setQuery(query %(graph_name, pagesize, the_offset))
    genes_uri = sparql_to_json(sparql)
    return genes_uri

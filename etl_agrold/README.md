# ETL AgroLD

Script d'extraction des informations sur les gènes à partir d'AgroLD.

### Prérequis et Installation

* La version de Python utilisée est [3.6.6](https://www.python.org/downloads/release/python-366/).
* Les packages suivants doivent être installés afin de tester le script:

| Package        |Version|                  From                     | Installation            |
|:--------------:|:-----:|:-----------------------------------------:|:-----------------------:|
| pip            |  18   |https://pypi.org/project/pip/              |pip install pip          |
| SPARQLWrapper  | 1.8.2 |https://rdflib.github.io/sparqlwrapper/    |pip install sparqlwrapper|


NOTE : Le module `pip` n'existe par défaut que dans les versions `2.7.9+` et `3.4+` de python, sinon il doit être installé manuellement.

### Le principe de fonctionnement du script:

* Récupérer la list des URI des gènes qui ont au moin un taxon.
* Grouper les URI des gènes (ex. batch de 80 gènes).
  * Pour chaque batch:
    * Pour chaque gène:
      * Récupérer toutes les informations associées à ce dernier (voir [les informations récupérées](#les-informations-récupérées) ci-dessous).
      * Générer le champ `description`.
      * Enregistrer les informations dans le CSV.

Le script génère un fichier CSV qui contient les informations suivantes:

* Pour chaque gène on a :
  * entry_type
  * database_name
  * db_id
  * db_version
  * description
  * url
  * species
  * xref
  * feature_type
  * sequence_id
  * sequence_version
  * start_position
  * end_position
  * map
  * map_position
  * authority
  * trait
  * trait_id
  * environment
  * environment_id
  * statistic
  * unit
  * genotype
  * experiment_type

Chaque ligne dans le fichier CSV correspond à un gène.

### Les informations récupérées:

Après l'interrogation de la base, et pour chaque gène on obtient un dictionnaire qui a la structure suivante:

* Gene ID
  * Gene URI
  * Gene name
  * Start position
  * End position
  * Description
  * Proteins
    * Protein ID
      * Protein URI
      * Protein name
      * Protein synonyms
      * Protein annotation
      * Protein description
      * Protein Locations
        * Protein Location ID
          * Protein Location URI
          * Protein Location name
          * Protein Location definition (IAO : Information Artifact Ontology)
        * ....
      * Protein Functions
        * Protein Function ID
          * Protein Function URI
          * Protein Function name (eg. Protein X has function A)
          * Protein Function definition (IAO)
        * ...
      * Protein Participations
        * Protein Participation ID
          * Protein Participation URI
          * Protein Participation name (eg. Protein X Participates in B)
          * Protein Participation definition (IAO)
        * ...
  * Taxons
    * Taxon ID
      * Taxon URI
      * Taxon name
  * Pathways
    * Pathway ID
      * Pathway URI
      * Pathway name
    * ...


1. Pour chaque gène, on peut avoir plusieurs `Proteins`, `Taxons` et `Pathways`.
2. Pour le moment on considère qu'un gène peut avoir plusieurs taxons (à modifier)
3. On prend tous les gènes avec et sans taxons (566807 dont 489299 genes qui ont au moins un taxon).
4. Le champ description est généré à partir des informations extraient de la base (gene name, start/end position, proteins, protein Locations, taxons, pathways etc).

#### Exemple du champ description:

    Gene ID AT5G41370 has URI http://identifiers.org/ensembl.plant/AT5G41370,
    Start position 16551119 and End position 16556038 on Chromosome 5
    It encodes protein(s): Q38861 (http://purl.uniprot.org/uniprot/Q38861).
    Function is 'DNA binding' (GO_0003677, http://purl.obolibrary.org/obo/GO_0003677:
      Function definition 'Any molecular function by which a gene product interacts selectively and non-covalently with DNA (deoxyribonucleic acid).').
    Location is 'nucleus' (GO_0005634, http://purl.obolibrary.org/obo/GO_0005634:
      Location definition 'A membrane-bounded organelle of eukaryotic cells in which chromosomes are housed and replicated. In most cells, the nucleus contains all of the cell's chromosomes except the organellar chromosomes, and is the site of RNA synthesis and processing. In some species, or in specialized cell types, RNA metabolism or DNA replication may be absent.').
    Participation in 'DNA duplex unwinding' (GO_0032508, http://purl.obolibrary.org/obo/GO_0032508:
      Participation definition 'The process in which interchain hydrogen bonds between two strands of DNA are broken or 'melted', generating a region of unpaired single strands.'
    Participation in 'response to UV' (GO_0009411, http://purl.obolibrary.org/obo/GO_0009411:
      Participation definition 'Any process that results in a change in state or activity of a cell or an organism (in terms of movement, secretion, enzyme production, gene expression, etc.) as a result of an ultraviolet radiation (UV light) stimulus. Ultraviolet radiation is electromagnetic radiation with a wavelength in the range of 10 to 380 nanometers.'
    Participation in 'nucleotide-excision repair' (GO_0006289, http://purl.obolibrary.org/obo/GO_0006289:
      Participation definition 'A DNA repair process in which a small region of the strand surrounding the damage is removed from the DNA helix as an oligonucleotide. The small gap left in the DNA helix is filled in by the sequential action of DNA polymerase and DNA ligase. Nucleotide excision repair recognizes a wide range of substrates, including damage caused by UV irradiation (pyrimidine dimers and 6-4 photoproducts) and chemicals (intrastrand cross-links and bulky adducts).'
    The species is 'Arabidopsis thaliana' (http://purl.obolibrary.org/obo/NCBITaxon_3702).

#!/usr/bin/python
#
#   See the README for more info
#
#   Requirement:
#       - python 3.x
#       - SPARQLWrapper (pip install sparqlwrapper )
###################################################################################

import sys
sys.path.append('../')
import knet_agro_common_functions as common_functions
common_functions.check_python_ver()
from SPARQLWrapper import SPARQLWrapper, JSON
import os.path
import csv
import json
import glob
import time
import pip
start_time = time.time()

AGROLD_ENDPOINT = 'http://volvestre.cirad.fr:8890/sparql'
AGROLD_URL = 'http://volvestre.cirad.fr:8890/describe/?url='
SOURCE = 'SouthGreen AgroLD'
ENTRY_TYPE = 'Gene annotation'
FEATURE_TYPE = 'Gene'
OUTPUT_PATH = "output_agrold" #"etl_agrold/output_agrold"
# Used for pagination (in LIMIT eg.1000)
PAGESIZE = 1000
# delay
SLEEP_TIME = 0.8
# Batch size (e.g 80 genes)
BATCH_SIZE = 80
# QUERIES PATH
QUERIES_PATH = "agrold_queries/"

def get_genes_info(endpoint, query, genes_uri_batch):
    """
    Get all the information related to group of genes.

    :param endpoint: the endpoint
    :param genes_uri_batch: the genes URIs batch (see 'set_batch' function)
    :returns: the result of the query
    """
    sparql = SPARQLWrapper(endpoint)
    sparql.setQuery(query %genes_uri_batch)
    genes_info = common_functions.sparql_to_json(sparql)
    return genes_info

def create_genes_uri_list(endpoint, nbr_of_genes, pagesize, query=True):
    """
    Create a list of genes URIs.

    :param choosed_db: the chosen database
    :param endpoint: the endpoint
    :param nbr_of_genes: the number of genes in db
    :param pagesize: specifies the size of a page of output.
        The value of 'pagesize' is the number of output lines to be produced
        used in the query (LIMIT pagesize).
    :param query: if it's 'True' the script execute the query on the endpoint,
        otherwise it reads the genes URIs directly from 'genes_uri.json' file
    :returns: the genes URIs list
    """
    genes_uri_list=[]
    if query:
        # Calculate the number of genes in the graph
        # (this will be used in pagination)
        the_offset = 0
        while(the_offset < nbr_of_genes + PAGESIZE):
             # extract all genes URI
            # get the query
            q_all_genes_uri = common_functions.read_query_file(QUERIES_PATH+'/get_all_genes_uri.rq')
            # execute it
            genes_uri = common_functions.get_all_genes_uri(endpoint, q_all_genes_uri, pagesize, the_offset)
            # Fill the lists
            for gene_uri in genes_uri:
                genes_uri_list.append(gene_uri["gene_uri"]["value"])
            the_offset += PAGESIZE
        common_functions.json_save(OUTPUT_PATH + '/genes_uri.json', genes_uri_list)
    else:
        genes_uri_list = common_functions.read_genes_uri_list(OUTPUT_PATH + '/genes_uri.json')
    print("There are %s genes to extract " %len(genes_uri_list))
    return genes_uri_list


def create_genes_info_dico(endpoint, genes_uri_batch):
    """
    Create the gene dictionary and it's corresponding information.

    :param endpoint: the endpoint
    :param genes_uri_batch: list of genes batches (see 'create_gene_batches' function)
    :returns: a list of all information about genes, retrived and stored in a dictionary
    """
    # Get the infos
    q_genes_info = common_functions.read_query_file(QUERIES_PATH + '/get_genes_info.rq')
    genes_info = get_genes_info(endpoint, q_genes_info, genes_uri_batch)
    # List of genes info dictionarries
    genes_info_list = []
    # dictinnary in which we store info about one gene
    gene_info_dico = {"gene_id":""}
    for info in genes_info:
        if "gene" in info:
            # get the URI
            gene_uri = info["gene"]["value"]
            # Get geneID (which will be the filename of JSON file too)
            # rsplit : Cuts the URI, join : joins the name space to the ID
            # replace : replaces the '.' with '_', upper : to capitale latters
            # e.g if uri = "http://www.southgreen.fr/agrold/tigr.locus/LOC_Os01g01040.1"
            # gene_id will be "TIGR_LOCUS_LOC_OS01G01040_1"
            gene_id = '_'.join(gene_uri.rsplit('/', 2)[-2:]).replace(".","_").upper()
            if gene_id not in gene_info_dico["gene_id"]:
                # if gene_info_dico contains something and has "gene_id"
                # put its content in genes_info_list before emptying it again
                if gene_info_dico and gene_info_dico["gene_id"] != "": # if gene_info_dico contains something
                    # Store the content in genes_info_list
                    genes_info_list.append(gene_info_dico)
                    gene_info_dico = {"gene_id":""}
                # start filling the dictionary
                gene_info_dico["gene_id"] = gene_id
                gene_info_dico["gene_URI"] = gene_uri

            # Add the gene_name, start_pos, end_pos et chromosome if they exist
            gene_general_attribs = ["gene_name", "start_pos", "end_pos", "chromosome"]
            common_functions.add_gene_element_to_dico(gene_general_attribs, info, gene_info_dico)

            # TAXONS
            # We consider that a gene may have many taxon names (to check out later)
            # Add taxon if it exists
            common_functions.add_generic_element_to_dico("taxon", ["taxon_name"], info, gene_info_dico, endpoint = "AgroLD")

            # PROTEINS
            prot_general_attribs = ["prot_name", "prot_syno", "prot_anno", "prot_desc"]
            prot_id = common_functions.add_generic_element_to_dico("protein", prot_general_attribs, info, gene_info_dico, endpoint = "AgroLD")
            # if prot_id is not empty (protein exists)
            if prot_id is not None:
                # protein location info
                prot_loc_attribs = ["prot_loc_name", "prot_loc_def"]
                common_functions.add_generic_element_to_dico("prot_loc", prot_loc_attribs, info, gene_info_dico["proteins"][prot_id], endpoint = "AgroLD")
                # protein function info
                prot_func_attribs = ["prot_func_name", "prot_func_def"]
                common_functions.add_generic_element_to_dico("prot_func", prot_func_attribs, info, gene_info_dico["proteins"][prot_id], endpoint = "AgroLD")
                # protein participation info
                prot_parti_attribs = ["prot_parti_name", "prot_parti_def"]
                common_functions.add_generic_element_to_dico("prot_parti", prot_parti_attribs, info, gene_info_dico["proteins"][prot_id], endpoint = "AgroLD")

            # PATHWAYS
            common_functions.add_generic_element_to_dico("pathway", ["pathway_name"], info, gene_info_dico, endpoint = "AgroLD")

    # Check if the gene_info_dico is not empety
    # which mean that the query returns nothing
    # e.g this gene :
    # <http://www.southgreen.fr/agrold/tigr.locus/LOC_Os01g01040.1>
    if gene_info_dico:
        # Add the last one before returning
        genes_info_list.append(gene_info_dico)
    # idea: else, save the lost genes batches in JSON file
    return genes_info_list

def create_gene_description_field(gene_info):
    """
    Create the description field based on the retrieved information about the gene.

    :param gene_info: the gene dictionary (full with info)
    :returns: the description (long string)
    """
    desc = ""
    # Gene info
    desc += "Gene ID %s has URI %s" %(gene_info["gene_id"], gene_info["gene_URI"])
    if "start_pos" in gene_info:
        desc += ", Start position %s " % gene_info["start_pos"][0]
    if "end_pos" in gene_info:
        desc += "and End position %s " % gene_info["end_pos"][0]
    if "chromosome" in gene_info:
        desc += "on Chromosome %s " % gene_info["chromosome"][0]
    # take only the dictionaries
    if isinstance(gene_info, dict):
        for gn_dic_key, gn_dic_val in gene_info.items():
            # Proteins info
            if isinstance(gn_dic_val, dict) and "proteins" in gn_dic_key:
                if gn_dic_val: desc += ". It encodes protein(s):"
                for pr_id, pr_info in gn_dic_val.items():
                    desc += " %s (%s)"%(pr_id, pr_info["protein_uri"])
                    #if pr_info["prot_syno"]:
                    if "prot_syno" in pr_info:
                        desc += "which has synonym(s) '%s' "%','.join(pr_info["prot_syno"])
                    if "prot_anno" in pr_info:
                        desc += "and annotaion(s) '%s' "%','.join(pr_info["prot_anno"])
                    if "prot_desc" in pr_info:
                        desc += "and description '%s' "%','.join(pr_info["prot_desc"])
                    # Protein functions info
                    for pr_dic_key, pr_dic_val in pr_info.items(): # e.g for each element in J3NC02 (func, loc and parti)
                        if isinstance(pr_dic_val, dict) and "prot_funcs" in pr_dic_key:
                            for pr_func_id, pr_func_info in pr_dic_val.items():
                                desc += ". Function is '%s' " %pr_func_info["prot_func_name"][0]
                                desc += "(%s, %s: " %(pr_func_id, pr_func_info["prot_func_uri"])
                                desc += "Function definition '%s')" %pr_func_info["prot_func_def"][0]

                        # Protein locations info
                        if isinstance(pr_dic_val, dict) and "prot_locs" in pr_dic_key:
                            for pr_loc_id, pr_loc_info in pr_dic_val.items():
                                desc += ". Location is '%s' " %pr_loc_info["prot_loc_name"][0]
                                desc += "(%s, %s: " %(pr_loc_id, pr_loc_info["prot_loc_uri"])
                                desc += "Location definition '%s')" %pr_loc_info["prot_loc_def"][0]

                        # Protein participations info
                        if isinstance(pr_dic_val, dict) and "prot_partis" in pr_dic_key:
                            for pr_parti_id, pr_parti_info in pr_dic_val.items():
                                desc += ". Participation in '%s' " %pr_parti_info["prot_parti_name"][0]
                                desc += "(%s, %s: " %(pr_parti_id, pr_parti_info["prot_parti_uri"])
                                desc += "Participation definition '%s' " %pr_parti_info["prot_parti_def"][0]

            # Taxons info
            if isinstance(gn_dic_val, dict) and "taxons" in gn_dic_key:
                for tx_id, tx_info in gn_dic_val.items():
                    desc += "The species is '%s' (%s)"%(','.join(tx_info["taxon_name"]), tx_info["taxon_uri"])

            # Pathways info
            if isinstance(gn_dic_val, dict) and "pathways" in gn_dic_key:
                for pw_id, pw_info in gn_dic_val.items():
                    desc += ". Part of pathway '%s' (%s, %s) "%(','.join(pw_info["pathway_name"]), pw_id, pw_info["pathway_uri"])
    return desc

def concat_taxons_names(taxons_dico):
    """
    Concatenate taxons names of a gene (if there are many).

    :param taxons_dico: the taxon dictionary which conatain the taxon name(s)
    :returns: the taxon names as a comma separated string
    """
    taxons_names = set()
    for taxon_id in taxons_dico:
        taxon_name = ','.join(taxons_dico[taxon_id]["taxon_name"])
        taxons_names.add(taxon_name)
    return ','.join(taxons_names)

def _main():
    common_functions.import_or_install('SPARQLWrapper')
    # Get the path of the current directory under which the .py file is executed
    work_dir = os.path.dirname(os.path.realpath(__file__))
    # create the path to output_dir
    path_dir = os.path.join(work_dir, OUTPUT_PATH)
    common_functions.create_output_dir(path_dir)
    # Remove the old files
    extension = ["csv"] # ["json", "csv"]
    common_functions.remove_files(OUTPUT_PATH, extension)
    # Count the number of genes
    # get and execute the query
    q_nbr_of_genes = common_functions.read_query_file(QUERIES_PATH + '/get_nbr_of_genes.rq')
    nbr_of_genes = common_functions.get_nbr_of_genes(AGROLD_ENDPOINT, q_nbr_of_genes)
    # Get All genes URI
    genes_uri_list = create_genes_uri_list(AGROLD_ENDPOINT, nbr_of_genes, PAGESIZE, query=False)
    # get genes uri batches
    genes_uri_batches = common_functions.create_gene_batches(genes_uri_list, BATCH_SIZE)
    # Opening the CSV file
    with open(os.path.join(OUTPUT_PATH, 'agrold_genes.csv'), 'w', newline='') as f:   # for Python 3.x
    #with open(os.path.join(OUTPUT_PATH, 'agrold_genes.csv'), 'wb') as f:   # for Python 2.x
        w = csv.writer(f, delimiter=',', quoting=csv.QUOTE_ALL, quotechar='\"')
        # Get the genes infos associated with each gene_uri
        for batch in genes_uri_batches:
            genes_batch_dico = create_genes_info_dico(AGROLD_ENDPOINT, batch)
            for gene_dico in genes_batch_dico:
                gene_id = list(gene_dico.keys())[0]
                # Get the description field
                gene_desc = create_gene_description_field(gene_dico)
                # Add the discription field
                gene_dico["description"] = gene_desc
                # get taxons names
                taxons_names = concat_taxons_names(gene_dico["taxons"])
                # set the url
                gene_url = AGROLD_URL + gene_dico['gene_URI']
                # Create empty start_pos and end_pos if they doesn't exist
                start_pos = gene_dico['start_pos'][0] if 'start_pos' in gene_dico else ""
                end_pos = gene_dico['end_pos'][0] if 'end_pos' in gene_dico else ""
                # Create the row
                row = [ ENTRY_TYPE,                             # entry_type
                        SOURCE,                                 # database_name
                        gene_id,                                # db_id
                        1,                                      # db_version
                        gene_dico['description'],               # description
                        gene_url,                               # url
                        taxons_names,                           # species
                        "",                                     # xref
                        FEATURE_TYPE,                           # feature_type
                        "",                                     # sequence_id
                        "",                                     # sequence_version
                        start_pos,                              # start_position
                        end_pos,                                # end_position
                        "",                                     # map
                        "",                                     # map_position
                        "",                                     # authority
                        "",                                     # trait
                        "",                                     # trait_id
                        "",                                     # environment
                        "",                                     # environment_id
                        "",                                     # statistic
                        "",                                     # unit
                        "",                                     # genotype
                        ""                                      # experiment_type
                    ]
                # Write/append it to the CSV file
                w.writerow(row)

            time.sleep(SLEEP_TIME)

    print("--- The extraction took %s seconds ---" % (time.time() - start_time))

if __name__ == "__main__": _main()

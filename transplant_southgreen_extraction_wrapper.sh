#!/bin/bash
#
# transplant_southgreen_extraction_wrapper.sh
#
# Script used to extract data from SouthGreen and index to SolR.
#
# Author: F. PHILIPPE
#
# Copyright INRA-URGI 2017
#


check_jq() {
    which jq > /dev/null
    [ $? -ne 0 ] && echo "JQ is not installed on this server, or not specified in PATH: " && echo "$PATH" && echo "Please install it via package manager or via tarball: https://stedolan.github.io/jq/download/. Exiting." && exit 1

    JQ_VERSION=$(jq --version)
    [ "${JQ_VERSION}" != "jq-1.5" ] && echo "JQ version expected is jq-1.5. Please use this version only. Exiting." && exit 1
}
check_jq


SOLR_HOST="10.0.0.133"
SOLR_PORT="8983"
CORE_NAME="URGI"
INDEXING=0
VERBOSE=0
VERBOSE_OPTION=""

WORK_DIR=""
INDEX_DIR="etl_agrold/output_agrold"
INDEXING_SCRIPT="./index_solr.sh"
SOUTHGREEN_EXTRACTION_SCRIPT="etl_agrold/agrold_extraction_genes.py"

usage() {
    cat <<EOF

USAGE :
    $0 [--index -solr_host <SOLR_HOST>] [-solr_port <SOLR_PORT>] [-solr_core <SOLR_CORE>]] [-source_name] [-v[v]] [-h|--help]

DESCRIPTION :
    Script used to fetch and index data from AgroLD project.

PARAMS:
    --index               index extracted data into a SolR instance (DEV instance by default or PROD instance if required)
    -solr_host            the host of the Solr instance to use for indexation (only with --index, DEFAULT: ${SOLR_HOST})
    -solr_port            the port of the Solr instance to use for indexation (only with --index, DEFAULT: ${SOLR_PORT})
    -solr_core		  the core used on Solr (DEFAULT: ${CORE_NAME})
    -source_name          the name of the source field of the index (DEFAULT: ${SOURCE_NAME})
    -v                    display verbose informations
    -vv                   display very verbose informations
    -h or --help          print this help

EOF
    exit 1
}

prerequesite(){
    REQUIRED="$1"
    [ -z "$1" ] && {
        echo "Required program is missing for checking. Exiting."
        exit 14
    }
    [ "$(which "${REQUIRED}" 2>/dev/null)" ] || {
        echo "ERROR: program $REQUIRED has not been found, please add it to your path and/or install it before running the script."
        _=$((MISSING_PREREQUESITE+=1))
    }
}

MISSING_PREREQUESITE=0
check_prerequesites(){
    prerequesite "pip"
    [ $MISSING_PREREQUESITE -gt 0 ] && {
        echo "Missing ${MISSING_PREREQUESITE} required dependencies. Please configure your environment accordingly. Now exiting."
        exit 14
    }
}
check_prerequesites

# get params
while [ -n "$1" ]; do
    case $1 in
        -h) usage;shift 1;;
        --help) usage;shift 1;;
        --index) INDEXING=1; shift 1;;
        -solr_host) SOLR_HOST=$2; shift 2;;
	-solr_port) SOLR_PORT=$2; shift 2;;
        -solr_core) SOLR_CORE=$2; shift 2;;
        -source_name) SOURCE_NAME=$2;shift 2;;
        -v) VERBOSE=1; VERBOSE_OPTION="-v";shift 1;;
        -vv) VERBOSE=2; VERBOSE_OPTION="-vv";shift 1;;
        --debug) set -x; shift 1;;
        --) shift;break;;
        -*) echo && echo "Unknown option: $1" && echo;exit 1;;
        *) break;;
    esac
done


check_error() {
    CODE=$1
    if [ "$CODE" -ne 0 ];then
        echo "Error $CODE occured!"
        echo "Exiting."
        exit 1
    fi
}

echo_and_eval_cmd() {
    local CMD=$1
    if [ -z "$CMD" ]; then
        echo "Missing command to eval. Exiting."
        exit 1
    fi
    local PRINTED_CMD="${CMD}"
    [ $VERBOSE -ge 1 ] && echo -e "Going to exec command: \n\t${PRINTED_CMD}"
    eval "$CMD"
    check_error $?
}

index() {
    cat <<EOF
================================================================================
               CSV indexation
================================================================================
EOF
    local DIR=$1
    [ -z "${DIR}" ] && "You must define where are located the files to index. Cannot index what I don't know..." && exit 1
    [ ! -d "${DIR}" ] && "${DIR} does not exist. Exiting..." && exit 1
    [ $VERBOSE -ge 2 ] && echo "Indexing data located in $DIR..."
    local CMD="$INDEXING_SCRIPT -host ${SOLR_HOST} -port ${SOLR_PORT} -c ${SOLR_CORE} -d ${DIR} ${VERBOSE_OPTION}"
    echo_and_eval_cmd "${CMD}"
    [ $VERBOSE -ge 2 ] && echo "Indexing done."
}

extract_data_from_json_to_csv(){
    cat <<EOF
====================================================================================
        Extract data from sparql endpoint and convert json to transplant csv
====================================================================================
EOF
    local CMD
    CMD="python ${SOUTHGREEN_EXTRACTION_SCRIPT}"
    echo_and_eval_cmd "${CMD}"
    [ $VERBOSE -ge 2 ] && echo "Extraction done."
}

main() {

    extract_data_from_json_to_csv
    if [ $INDEXING -eq 1 ]; then
        index $INDEX_DIR
    fi
}

main

echo "Finished."
exit 0

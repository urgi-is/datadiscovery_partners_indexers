#!/usr/bin/env bash


help="./get_all_remote_datadiscovery.sh , then copy the content of the publish folder to the mnt-data"
if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
    echo $help
    exit 0
fi

if [ -d "publish" ]; then
    rm -r ./publish/*
else
    mkdir publish
fi

# ################################################################ #
# Get e!DAL datadiscovery metadata from
# https://doi.ipk-gatersleben.de/faidare.json
# and save it to ./IPK/IPK_eDAL_datadiscovery.json
echo "Get e!DAL datadiscovery metadata from https://doi.ipk-gatersleben.de/faidare.json and save it to ./publish/IPK/IPK_eDAL_datadiscovery.json"
mkdir ./publish/IPK
curl https://doi.ipk-gatersleben.de/faidare.json --compressed -o ./publish/IPK/IPK_eDAL_datadiscovery.json


# ################################################################ #
# Get NIB/SKM datadiscovery metadata from
# curl https://skm.nib.si/downloads/pss/public/faidare
# curl https://skm.nib.si/downloads/ckn/faidare 
# and save it to ./NIB/NIB_SKM_datadiscovery.json
echo "Get NIB/SKM/PSS datadiscovery metadata from curl https://skm.nib.si/downloads/pss/public/faidare and save it to ./publish/NIB/NIB_SKM_PSS_datadiscovery.json"
mkdir ./publish/NIB
curl https://skm.nib.si/downloads/pss/public/faidare --compressed -o ./publish/NIB/NIB_SKM_PSS_datadiscovery.json
echo "Get NIB/SKM/CKN datadiscovery metadata from curl https://skm.nib.si/downloads/ckn/faidare and save it to ./publish/NIB/NIB_SKM_CKN_datadiscovery.json"
curl https://skm.nib.si/downloads/ckn/faidare --compressed -o ./publish/NIB/NIB_SKM_CKN_datadiscovery.json

echo "gzip everything"
gzip publish/*/*.json

#!/usr/bin/python
#
#   See the README for more info
#
#   Requirement:
#       - python 3.x
#       - SPARQLWrapper (pip install sparqlwrapper )
###################################################################################

import pip
import sys
import re
sys.path.append('../')
import knet_agro_common_functions as common_functions
common_functions.check_python_ver()
from SPARQLWrapper import SPARQLWrapper, JSON
import xml.etree.ElementTree as ET
import urllib.request as urllib
import os.path
import csv
import json
import time
import subprocess
start_time = time.time()
import datetime
now = datetime.datetime.now()


KNETMINER_ENDPOINT = 'http://localhost:8890/sparql/'
NCBI_TAXON_URL = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=taxonomy&id='
WHEAT_URL = "http://knetminer.rothamsted.ac.uk/wheatknet/genepage?list="
ARA_URL = "http://knetminer.rothamsted.ac.uk/araknet/genepage?list="
ENTRY_TYPE = 'Gene annotation'
DATABASE_NAME = 'KnetMiner'
NODE = 'Rothamsted Research'
# Path to queries files
QUERIES_PATH = "knetminer_queries"
# Used for pagination (in LIMIT eg.1000)
PAGESIZE = 10000
# Batch size (e.g 50 genes)
BATCH_SIZE = 50


def fetch_taxon_name(file, ncbi_url, taxon_id):
    """
    Fetch the taxon scientific name from a JSON file,
    if the taxon doesn't exist in the file then fetch the name
    from the provided url.

    :param file: the file which has the taxons' id and name
    :param ncbi_url: NCBI url used to get info about taxon
    :param taxon_id: the taxon id
    :returns: taxon name(s) corresponding to the provided taxon_id
    """
    # if the file exists open it
    if os.path.isfile(file):
        taxons_names_dict = common_functions.json_load(file)
    # else create the empty dict
    else:
        taxons_names_dict = {}

    if taxon_id not in taxons_names_dict:
        # the full url
        taxon_url = ncbi_url + taxon_id
        # fetch from web page
        try:
            tree = ET.parse(urllib.urlopen(taxon_url))
            for item in tree.findall('.//Item[@Name="ScientificName"]'):
                taxon_sci_name = item.text
                # add taxon id and value to taxons_names_dict
                taxons_names_dict[taxon_id] = taxon_sci_name
        except urllib.error.HTTPError:
            print("The server can't handle this amount of requests !!")
        # save the dict in JSON file
        common_functions.json_save(file, taxons_names_dict)
    return [taxons_names_dict[taxon_id]]


def get_genes_info(endpoint, graph_name, query, genes_uri_batch, pagesize, the_offset):
    """
    Get all the information related to group of genes.

    :param endpoint: the endpoint
    :param graph_name: the graph name used in FROM of the SPARQL query
    :param query: query from 'get_genes_info.rq' file
    :param genes_uri_batch: the genes URIs batch (see 'set_batch' function)
    :returns: infromation about the group of genes
    """
    sparql = SPARQLWrapper(endpoint)
    sparql.setQuery(query %(graph_name, genes_uri_batch, pagesize, the_offset))
    genes_info = common_functions.sparql_to_json(sparql)
    return genes_info


def create_genes_uri_list(chosen_db, endpoint, nbr_of_genes, pagesize, query=True):
    """
    Create a list of genes URIs.

    :param chosen_db: the chosen database
    :param endpoint: the endpoint
    :param nbr_of_genes: the number of genes in db
    :param pagesize: specifies the size of a page of output.
        The value of 'pagesize' is the number of output lines to be produced
        used in the query (LIMIT pagesize).
    :param query: if it's 'True' the script execute the query on the endpoint,
        otherwise it reads the genes URIs directly from 'genes_uri.json' file
    :returns: the genes URIs list
    """
    genes_uri_list=[]
    if query:
        offset = 0
        while(offset < nbr_of_genes + pagesize):
            # extract all genes URI
            # get the query
            q_all_genes_uri = common_functions.read_query_file(QUERIES_PATH + '/get_all_genes_uri.rq')
            # execute it
            genes_uri = common_functions.get_all_genes_uri(endpoint, chosen_db["GRAPH_NAME"], q_all_genes_uri, pagesize, offset)
            # Fill the lists
            for gene_uri in genes_uri:
                genes_uri_list.append(gene_uri["gene"]["value"])
            # move to the next page
            offset += pagesize
        # save genes URIs in JSON file
        common_functions.json_save(chosen_db["OUTPUT_PATH"]+'/'+chosen_db["GENES_URI_JSON"] ,genes_uri_list)
    else:
        # read directly the JSON file
        genes_uri_list = common_functions.read_genes_uri_list(chosen_db["OUTPUT_PATH"]+'/'+chosen_db["GENES_URI_JSON"])
    print("There are %s genes to extract " %len(genes_uri_list))
    return genes_uri_list


def create_genes_info_dico(chosen_db, endpoint, genes_uri_batch, nbr_of_genes, pagesize):
    """
    Create the gene dictionary and it's corresponding information.

    :param chosen_db: the chosen database
    :param endpoint: the endpoint
    :param genes_uri_batch: list of genes batches (see 'create_gene_batches' function)
    :returns: list of information about genes, retrived and stored in a dictionary
    """
    offset = 0
    # dictionnary in which we store info about one gene
    # initialized with an empty gene_id
    gene_info_dico = {"gene_id":""}
    # List of genes info dictionaries (genes_info_list = [gene_info_dico1, gene_info_dico2, ..])
    genes_info_list = []
    # Get the infos
    while(offset < nbr_of_genes + pagesize):
        # extract all genes infos
        # get the query
        q_genes_info = common_functions.read_query_file(QUERIES_PATH + '/get_genes_info.rq')
        # execute it
        genes_info = get_genes_info(endpoint, chosen_db["GRAPH_NAME"], q_genes_info, genes_uri_batch, pagesize, offset)
        # Fill the lists
        for info in genes_info:
            if "gene" in info:
                # get the URI
                gene_uri = info["gene"]["value"]
                # Get geneID (remove the http part and make the id cleaner)
                gene_id = gene_uri.split('/')[-1].replace("gene_","").upper()
                gene_id = re.sub('\_LOC[a-zA-Z0-9_.-]*', '', gene_id)
                # if gene_id value not in gene_info_dico["gene_id"]
                # gene_info_dico["gene_id"] can be either "" or "HasAnotherID"
                if gene_id not in gene_info_dico["gene_id"]:
                    # if gene_info_dico has "gene_id" (and it's not empty)
                    # put its content in genes_info_list before emptying it again
                    if gene_info_dico["gene_id"] != "":
                        # Store the content in genes_info_list
                        genes_info_list.append(gene_info_dico)
                        gene_info_dico = {"gene_id":""}
                    # start filling the dictionary
                    gene_info_dico["gene_id"] = gene_id
                    gene_info_dico["gene_URI"] = gene_uri
                    # here we group the taxons
                    gene_info_dico["taxons"] = {}

                # Add the gene_name, start_pos, end_pos et chromosome if they exist
                gene_general_attribs = ["gene_alt_name", "start_pos", "end_pos", "chromosome"]
                common_functions.add_gene_element_to_dico(gene_general_attribs, info, gene_info_dico)

                # set gene_name_id to 'unknown_source' url 'empty' by default (just in case)
                gene_name_id = "unknown_source" + ":" + gene_id
                gene_info_dico["url"] = ""
                # TAXONS
                # we removed 'Optional' from taxon in the query to make sure that
                # every retrieved gene has a taxon (142 out of 138068 genes doesn't,
                # e.g : http://www.ondex.org/bioknet/resources/gene_629385, 629543, 629559..)

                # Get taxonID
                taxon_id = info["taxon_id"]["value"]
                # Get taxon URL
                taxon_url = NCBI_TAXON_URL + taxon_id
                # if the taxon not alreasy exists
                if taxon_id not in gene_info_dico["taxons"]:
                    gene_info_dico["taxons"][taxon_id] = {}
                    gene_info_dico["taxons"][taxon_id]["taxon_url"] = taxon_url
                    gene_info_dico["taxons"][taxon_id]["taxon_name"] = fetch_taxon_name(chosen_db["OUTPUT_PATH"]+'/'+chosen_db["TAXONS_NAMES_JSON"], NCBI_TAXON_URL, taxon_id)
                    current_taxon = gene_info_dico["taxons"][taxon_id]["taxon_name"][0]

                # Since Wheat.ttl file contains both Arabido and Wheat species
                # and the gene_name_id depends on which specie he's belonging to;
                # we had to create a new field called 'gene_name_id' which is based on the taxon name
                # the URL also will depend on the taxon
                if current_taxon == "Arabidopsis thaliana":
                    gene_name_id = "araknet" + ":" + gene_id
                    gene_info_dico["url"] = ARA_URL + gene_id.split("_")[-1]

                elif current_taxon == "Triticum aestivum":
                    gene_name_id = "wheatknet" + ":" + gene_id
                    gene_info_dico["url"] = WHEAT_URL + gene_id.split("_")[-1]

                # add gene_name_id to the dictionnary
                gene_info_dico["gene_name_id"] = gene_name_id

                # PROTEINS
                prot_general_attribs = ["prot_pref_name", "prot_synonyms"]
                prot_id = common_functions.add_generic_element_to_dico("protein", prot_general_attribs, info, gene_info_dico)

                """
                # THIS PART IS COMMENTED/REMOVED DUE TO IT COMPLEXITY
                # TODO => TO CKECK IF WHE CAN ADD LATER
                if prot_id is not None:
                    prot_xref_attribs = ["xref_names"]
                    common_functions.add_generic_element_to_dico("xref", prot_xref_attribs, info, gene_info_dico["proteins"][prot_id])
                    # Enzymes
                    prot_enzyme_attribs = ["enzyme_pref_name"]
                    common_functions.add_generic_element_to_dico("enzyme", prot_enzyme_attribs, info, gene_info_dico["proteins"][prot_id])
                    # ortholog
                    prot_ortholog_attribs = ["ortholog_synonyms"] # AS OPTIONAL ALONE IN THE QUERY
                    common_functions.add_generic_element_to_dico("ortholog", prot_ortholog_attribs, info, gene_info_dico["proteins"][prot_id])
                    # Proteins which have similar sequences
                    prot_hss_attribs = []
                    common_functions.add_generic_element_to_dico("prot_has_sim_seq", prot_hss_attribs, info, gene_info_dico["proteins"][prot_id])
                """
                # BIO PROC
                bio_proc_general_attribs = ["bio_proc_alt_name", "bio_proc_desc", "bio_proc_go"]
                common_functions.add_generic_element_to_dico("bio_proc", bio_proc_general_attribs, info, gene_info_dico)

                """
                # CELL COMP and MOL FUNC are commented because they slow down the extraction
                # uncomment them if needed (uncomment them in the query file too)
                # CELL COMP
                cell_comp_general_attribs = ["cell_comp_alt_name", "cell_comp_desc", "cell_comp_go"]
                common_functions.add_generic_element_to_dico("cell_comp", cell_comp_general_attribs, info, gene_info_dico)

                # MOL FUNC
                mol_func_general_attribs = ["mol_func_alt_name", "mol_func_desc", "mol_func_go"]
                common_functions.add_generic_element_to_dico("mol_func", mol_func_general_attribs, info, gene_info_dico)
                """
                # phenotype
                pheno_general_attribs = ["phenotype_name", "phenotype_desc"]
                common_functions.add_generic_element_to_dico("phenotype", pheno_general_attribs, info, gene_info_dico)

                # TO
                TO_general_attribs = ["TO_pref_name", "TO_trait", "TO_desc", "TO_trait_name"]
                common_functions.add_generic_element_to_dico("TO", TO_general_attribs, info, gene_info_dico)

                # SNP
                SNP_general_attribs = ["SNP_pos"]
                common_functions.add_generic_element_to_dico("SNP", SNP_general_attribs, info, gene_info_dico)
        # move to the next page
        offset += pagesize

    # Check if the gene_info_dico is empty
    # which mean that the query return nothing
    # e.g this gene :
    # <http://www.ondex.org/bioknet/resources/gene_312588>
    if gene_info_dico:
        # Add the last one before returning
        genes_info_list.append(gene_info_dico)
    # IDEA: else, save the lost genes batches in JSON file
    return genes_info_list


def create_gene_description_field(gene_info):
    """
    Create the description field based on the retrieved information about the gene.

    :param gene_info: the gene dictionary (filled with info)
    :returns: the description (long string)

    TODO :
        This function contains four levels of for loop, which is quite big.
        The proposed idea is to create dedicated objects describing Gene
        and related (Taxon, Protein, Xref, etc., replacing the dictionary)
        and delegating to theses classes the process of formatting its gene's, taxon's, protein's descriptions.
        That could be later tested in an easier way, method by method to be sure that each sub-description
        (one for taxons, one for proteins...) are correctly handled.
    """
    desc = ""
    # Gene info
    desc += "Gene ID %s" %gene_info["gene_id"]
    if "gene_alt_name" in gene_info:
        desc += " has alternative name(s): %s" %', '.join(gene_info["gene_alt_name"])
    if "start_pos" in gene_info:
        desc += ", Start position %s" % gene_info["start_pos"][0]
    if "end_pos" in gene_info:
        desc += " and End position %s" % gene_info["end_pos"][0]
    if "chromosome" in gene_info:
        desc += " on Chromosome %s," % gene_info["chromosome"][0]
    # take only the dictionaries
    if isinstance(gene_info, dict):
        for gn_dic_key, gn_dic_val in gene_info.items():
            # Taxons info
            if isinstance(gn_dic_val, dict) and "taxons" in gn_dic_key:
                for tx_id, tx_info in gn_dic_val.items():
                    desc += " The species is '%s' (%s)"%(','.join(tx_info["taxon_name"]), tx_info["taxon_url"])
            # Proteins info
            if isinstance(gn_dic_val, dict) and "proteins" in gn_dic_key:
                if gn_dic_val: desc += ". It encodes protein(s):"
                for pr_id, pr_info in gn_dic_val.items():
                    desc += " %s"%pr_id
                    if "prot_pref_name" in pr_info:
                        desc += ", has prefered name: '%s'"%','.join(pr_info["prot_pref_name"])
                    if "prot_synonyms" in pr_info:
                        desc += ", Which has synonym(s): '%s'"%','.join(pr_info["prot_synonyms"])
                    # TODO decide whether we add this part or not
                    """
                    for pr_dic_key, pr_dic_val in pr_info.items(): # e.g for each element in J3NC02 (p_s_s, xref...)
                        # Proteins with similar sequence (# to remove ?)
                        if isinstance(pr_dic_val, dict) and "prots_has_sim_seq" in pr_dic_key:
                            if pr_dic_val : desc += ". Protein(s) with similar sequence : "
                            for pr_s_s_id, pr_s_s_info in pr_dic_val.items():
                                desc += " %s" %pr_s_s_id
                                desc += " (%s)," %pr_s_s_info["prot_has_sim_seq_uri"]
                        # xrefs
                        if isinstance(pr_dic_val, dict) and "xrefs" in pr_dic_key:
                            if pr_dic_val : desc += " Xref :"
                            for xref_id, xref_info in pr_dic_val.items():
                                desc += " names '%s'" %','.join(xref_info["xref_names"])
                                desc += " (ID: %s" %xref_id
                                desc += ", URI: %s)." %xref_info["xref_uri"]
                    """
            # Biological Process
            if isinstance(gn_dic_val, dict) and "bio_proc" in gn_dic_key:
                if gn_dic_val: desc += ". It participates in the following biological process(es):"
                for bio_proc_id, bio_proc_info in gn_dic_val.items():
                    desc += " %s"%bio_proc_id
                    desc += " has alternative name(s) '%s'"%', '.join(bio_proc_info["bio_proc_alt_name"])
                    if "bio_proc_desc" in bio_proc_info:
                        desc += " (biological process description : %s)."%bio_proc_info["bio_proc_desc"][0].replace('\n', "")

            """
            # Cellular Component
            if isinstance(gn_dic_val, dict) and "cell_comp" in gn_dic_key:
                if gn_dic_val: desc += ". It's located in (cellular component): "
                for cell_comp_id, cell_comp_info in gn_dic_val.items():
                    desc += " %s"%cell_comp_id
                    desc += " has alternative name(s) '%s'"%', '.join(cell_comp_info["cell_comp_alt_name"])
                    if "cell_comp_desc" in cell_comp_info:
                        desc += " (cellular component description : %s)."%cell_comp_info["cell_comp_desc"][0]

            # Molecular Function
            if isinstance(gn_dic_val, dict) and "mol_func" in gn_dic_key:
                if gn_dic_val: desc += ". has function (molecular function): "
                for mol_func_id, mol_func_info in gn_dic_val.items():
                    desc += " %s"%mol_func_id
                    desc += " has alternative name(s) '%s'"%', '.join(mol_func_info["mol_func_alt_name"])
                    if "mol_func_desc" in mol_func_info:
                        desc += " (molecular function description : %s)."%mol_func_info["mol_func_desc"][0]
            """

            # Phenos
            if isinstance(gn_dic_val, dict) and "phenotypes" in gn_dic_key:
                if gn_dic_val: desc += ". It is related to the phenotype(s):"
                for pheno_id, pheno_info in gn_dic_val.items():
                    desc += " '%s'" %','.join(pheno_info["phenotype_name"])
                    desc += " (ID: %s)"%pheno_id
                    if "phenotype_desc" in pheno_info:
                        desc += " (phenotype description : %s)."%pheno_info["phenotype_desc"][0]
            # TOs
            if isinstance(gn_dic_val, dict) and "TOs" in gn_dic_key:
                if gn_dic_val: desc += " It is annotated/related with trait ontology(ies):"
                for TO_id, TO_info in gn_dic_val.items():
                    desc += " '%s' TO" %','.join(TO_info["TO_pref_name"])
                    desc += " (TO ID : %s" %TO_id
                    desc += ", TO description : %s)"%TO_info["TO_desc"][0]
                    desc += ", TO trait name '%s'" %','.join(TO_info["TO_trait_name"])
                    desc += " (%s)." %','.join(TO_info["TO_trait"])
                    # add the cooc_wi genes ?
            # SNPs
            if isinstance(gn_dic_val, dict) and "SNPs" in gn_dic_key:
                if gn_dic_val: desc += ". It has SNP(s):"
                for SNP_id, SNP_info in gn_dic_val.items():
                    desc += " %s"%SNP_id
                    desc += " in position %s" %SNP_info["SNP_pos"][0]
                    #desc += " '%s'"%','.join(SNP_info["SNP_pref_name"])
                    #desc += " SNP trait name '%s'" %','.join(SNP_info["SNP_trait_name"])
                    #desc += " (%s)." %','.join(SNP_info["SNP_trait"])
    return desc


def concat_taxon_names(taxons_dico):
    """
    Concatenate taxon names in one string (comma separated values).

    :param taxons_dico: the taxon dictionary which has a taxon id as key and
        one or many taxon name(s)
    :returns: taxon name(s) separated by comma
    """
    taxons_names = set()
    for taxon_id in taxons_dico:
        taxon_name = ','.join(taxons_dico[taxon_id]["taxon_name"])
        taxons_names.add(taxon_name)
    return ','.join(taxons_names)

def split_by_species(big_file_path):
    """
    Split the extracted info (saved in one big csv file) by species

    Note: This function is temporary since it's used to separate
    Arabidopsis from Wheat.

    :param big_file_path: the path to the file containing Arabidopsis and Wheat mixed together
    (e.g: big_file_path = output_knetminer/output_wheat/knetminer_wheat_genes.csv)
    :returns: list of the created file paths, it will be used when converting to json and
    compressing the output
    """
    base_dir = os.path.dirname(big_file_path)
    file_paths_list = []
    separator = ','
    quote = '"'
    with open(big_file_path) as fin:
        csvin = csv.DictReader(fin)
        outputs = {}
        for row in csvin:
            specie = row['species']
            # Open a new file and write the header
            if specie not in outputs:
                # Refine the specie's name
                specie_name = specie.replace(" ", "_").lower()
                full_file_path = base_dir+'/knet_{}_{}.csv'.format(specie_name, now.strftime("%Y%m%d"))
                # Remove extension
                file_path, file_extension = os.path.splitext(full_file_path)
                # Append the file path (without extension) to file_paths_list
                file_paths_list.append(file_path)
                fout = open(full_file_path, 'w')
                dw = csv.DictWriter(
                    fout,
                    fieldnames=csvin.fieldnames,
                    delimiter=separator,
                    quotechar=quote,
                    quoting=csv.QUOTE_NONNUMERIC
                )
                dw.writeheader()
                outputs[specie] = fout, dw
            # Always write the row
            outputs[specie][1].writerow(row)
        # Close all the files
        for fout, _ in outputs.values():
            fout.close()
        # Remove the original/big csv file
        os.remove(big_file_path)
        return file_paths_list


def _main():
    common_functions.import_or_install('SPARQLWrapper')
    # Ask the user to choose a database
    chosen_db = common_functions.choose_db(common_functions.AVAILABLE_DBS)
    # Get the path of the current directory under which the .py file is executed
    work_dir = os.path.dirname(os.path.realpath(__file__))
    # create the path to output_dir
    path_dir = os.path.join(work_dir, chosen_db["OUTPUT_PATH"])
    common_functions.create_output_dir(path_dir)
    # Remove the old JSONs file
    extension = ["csv"] # ["json", "csv"]
    common_functions.remove_files(chosen_db["OUTPUT_PATH"], extension)
    # Count the number of genes
    # Get and execute the query
    q_nbr_of_genes = common_functions.read_query_file(QUERIES_PATH + '/get_nbr_of_genes.rq')
    nbr_of_genes = common_functions.get_nbr_of_genes(KNETMINER_ENDPOINT, chosen_db["GRAPH_NAME"], q_nbr_of_genes)
    # Get All genes URI
    # If the query parameter set to False, the script will directly read the
    # content of 'arabido_genes_uri.json' file which has the list of genes
    genes_uri_list = create_genes_uri_list(chosen_db, KNETMINER_ENDPOINT, nbr_of_genes, PAGESIZE) # query=False
    # Get genes uri batches
    genes_uri_batches = common_functions.create_gene_batches(genes_uri_list, BATCH_SIZE)
    # get the file path
    full_file_path =  os.path.join(chosen_db["OUTPUT_PATH"], chosen_db["GENES_CSV"])
    # Opening the CSV file
    with open(full_file_path, 'w', newline='') as f:   # for Python 3.x
    #with open(os.path.join(OUTPUT_PATH, 'knetminer_arabido_genes.csv'), 'wb') as f:   # for Python 2.x
        w = csv.writer(f, delimiter=',', quoting=csv.QUOTE_ALL, quotechar='\"')
        # add the header
        w.writerow(["entryType", "name", "databaseName", "description", "url", "species","node"])
        # Get the genes infos associated with each gene_uri
        i = 1
        for batch in genes_uri_batches:
            genes_batch_dico = create_genes_info_dico(chosen_db, KNETMINER_ENDPOINT, batch, nbr_of_genes, PAGESIZE)
            for gene_dico in genes_batch_dico:
                gene_id = gene_dico["gene_id"]
                # gene_name_id and gene_url depend on whether the gene is
                # coming from either Arabido or Wheat,
                # and since the provided Wheat.ttl file contains both Arabido and Wheat
                # we retrieve both values in the moment when we know the taxon name
                # see '# TAXONS' part in create_genes_info_dico() function
                gene_name_id = gene_dico["gene_name_id"]
                gene_url = gene_dico["url"]
                # Get the description field
                gene_desc = create_gene_description_field(gene_dico)
                # get taxons names
                taxons_names = concat_taxon_names(gene_dico["taxons"])

                # Create the row
                row = [ ENTRY_TYPE,                             # entryType
                        gene_name_id,                           # name
                        DATABASE_NAME,                          # databaseName
                        gene_desc,                              # description
                        gene_url,                               # url
                        taxons_names,                           # species
                        NODE,                                   # node
                    ]
                # Write/append it to the CSV file
                w.writerow(row)
                print("\r"+str(i)+"/"+str(nbr_of_genes)+" Genes extracted.", end='')
                i += 1

    print("\n--- The extraction took %s seconds ---" % (time.time() - start_time))

    # split by species after the extraction
    print("Splitting file...")
    species_file_path_list = split_by_species(full_file_path)

    print("Converting CSV to JSON and Compressing files...")
    for file_path in species_file_path_list:
        jqCommand = "jq -R -s -f csv2json_knetminer.jq {} > {}" .format(file_path+'.csv', file_path+'.json')
        output = subprocess.check_output(['bash','-c', jqCommand])
        gzipCommand = "gzip {} {}" .format(file_path+'.csv', file_path+'.json')
        output = subprocess.check_output(['bash','-c', gzipCommand])

    print("Done!")

if __name__ == "__main__": _main()

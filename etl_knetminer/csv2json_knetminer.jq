# USAGE:
# jq -R -s -f csv2json_knet.jq csv_file.csv > new_json_file.json
# $ jq -R -s -f csv2json_knet.jq knetminer_wheat_genes.csv > knetminer_wheat_genes.json
# Requires jq 1.5+

# replaces the leading and trailing blank only once, only for strings
def trimq:
  if type == "string"
    then (.|sub("^ +";"") | sub(" +$";""))
    else .
  end
;

# to_array_if_needed/1 splits the a string on comma separator
# (only in case of species) to construct the array
def to_array_if_needed(header):
  if type == "string"
      #and (.|index(",") != null)
      and header == "species"
    then ( . | [split(",")[]| trimq ] )
    else .
  end
  ;

# objectify/1 takes an array of string values as inputs, converts
# numeric values to numbers, and packages the results into an object
# with keys specified by the "headers" array
def objectify(headers):
  def tonumberq: .;
  def tonullq: if . == "" then null else . end;
  . as $in
  | reduce range(0; headers|length) as $i (
    {}; headers[$i] as $header
    | .[headers[$i]] = (
      $in[$i]
      | to_array_if_needed($header)
      | tonumberq
      | tonullq
      )
    )
    ;

def csv2table:
  def transform: gsub(",,";",\"\",") ;                                         # add 2 double quotes between 2 commas, so that next split produces a correct column with the used field separator: quotes comma quotes
  def trim: gsub("\"";"") | sub("^ +";"") |  sub(" +$";"") | sub("\r";"");     # remove all leading and trailing spaces and also inner double quotes
  split("\n")
  | map( transform
  | split("\",\"")
  | map(trim) );

def csv2json:
  csv2table
  | .[0] as $headers
  | reduce (.[1:][] | select(length > 0) ) as $row
      ( []; . + [ $row|objectify($headers) ]);

csv2json

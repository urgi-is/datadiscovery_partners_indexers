# ETL KNetMiner

Script d'extraction des informations sur les gènes à partir de KNetMiner.

## Prérequis et Installation

* Version de Python requise : 3.x.
* Les packages suivants doivent être installés afin de tester le script:

| Package        |Version|                  From                     | Installation            |
|:--------------:|:-----:|:-----------------------------------------:|:-----------------------:|
| pip            |  18   |https://pypi.org/project/pip/              |pip install pip          |
| SPARQLWrapper  | 1.8.2 |https://rdflib.github.io/sparqlwrapper/    |pip install sparqlwrapper|

Les fichiers fournis (`WheatKNET-17072018.ttl` et `ArabidopsisKNET-17072018.ttl`) doivent être chargés dans un triplestore afin de créer un endpoint. Le triplestore utilisé est [Virtuoso](https://github.com/openlink/virtuoso-opensource).

### Installation de Virtuoso en local

#### Package Dependencies

Afin de générer le script de configuration et tous les autres fichiers de compilation nécessaires, veuillez vous assurer que les packages suivants et les versions recommandées (au moins les versions indiquées dans le tableau) sont installés sur votre système :

| Package   |Version |                  From                     |Check version using |
|:---------:|:------:|:-----------------------------------------:|:------------------:|
| autoconf  | 2.57   |http://www.gnu.org/software/autoconf/      |autoconf --version  |
| automake  | 1.9    |http://www.gnu.org/software/automake/      |automake --version  |
| libtool   | 1.5.16 |http://www.gnu.org/software/libtool/       |libtoolize --version|
| flex      | 2.5.33 |http://www.gnu.org/software/non-gnu/flex/  |flex --version      |
| bison     | 2.5.33 |http://www.gnu.org/software/bison/         |bison --version     |
| gperf     | 2.7.2  |http://www.gnu.org/software/gperf/         |gperf --version     |
| gawk      | 3.1.1  |http://www.gnu.org/software/gawk/          |gawk --version      |
| m4        | 1.4.1  |http://www.gnu.org/software/m4/            |m4 --version        |
| make      | 3.79.1 |http://www.gnu.org/software/make/          |make --version      |
| OpenSSL   | 0.9.8e |http://www.openssl.org/                    |openssl version     |


NOTE : Si vous rencontrez [ce type d'erreur](https://github.com/openlink/virtuoso-opensource/issues/663) à cause de la version 1.1 d'OpenSSL, vous pouvez forcer l'installation de OpenSSL 1.0 on utilisant la commande `sudo apt install libssl1.0-dev` sur Ubuntu ou `sudo dnf install compat-openssl10-devel --allowerasing` pour Fedora.

#### Espace Disque Requis

Le build produit une base de données de démonstration et des packages d'application Virtuoso assez volumineux. Au moins 800 Mo d'espace libre devraient être disponibles dans le système, en plus de 10 Go pour le chargement des deux fichiers turtle.

#### Installation

Vous clonez la version [develop/7](https://github.com/openlink/virtuoso-opensource):

    git clone https://github.com/openlink/virtuoso-opensource.git

Après vous exécutez les commandes suivants:

    ./autogen.sh
    CC=cc
    CFLAGS="-O2 -m64"
    export CFLAGS CC
    ./configure
    make
    make install

#### Démarrage de serveur

Vous pouvez maintenant démarrer le serveur en arrière-plan avec la commande `virtuoso-t -f &`:

    cd $HOME/virtuoso/var/lib/virtuoso/db
    virtuoso-t -f &

Après on peut accéder à :
* l'interface graphique : http://localhost:8890/
* l'endpoint : http://localhost:8890/sparql/

NOTE : Le login et le mot de passe par défaut : (Account : `dba`, Password : `dba`)

Source : [Le README de Virtuoso](https://github.com/openlink/virtuoso-opensource/blob/develop/7/README).

#### Chargement des fichiers

#### En utilisant l'interface graphique (Conductor)
1. Allez à l'interface de Conductor : http://localhost:8890/conductor
2. Naviguez vers `Linked Data` -> `Quad Store Upload`
3. Chargez le fichier désiré (ex. `WheatKNET-17072018.ttl`)
4. Précisez le nom du graphe dans le champ `Named Graph IRI`:
  - Dans le cas de `WheatKNET-17072018.ttl` le nom du graphe sera `http://latest.knetminer.wheat.ac.uk`.
  - Dans le cas de `ArabidopsisKNET-17072018.ttl` le nom du graphe sera `
http://latest.knetminer.arabidopsis.ac.uk`.

#### En utilisant la ligne de commande (Bulk Loader)
1. On verifie que le répertoire contenant les fichiers `ttl.gz` ou `ttl` est inclus dans le paramètre `DirsAllowed` défini dans le fichier `virtuose.ini`.

       DirsAllowed = ., /home/bilal/git/virtuoso/share/virtuoso/vad

2. On lance `isql`:

       $ isql 1111 dba dba
       OpenLink Virtuoso Interactive SQL (Virtuoso)
       Version 07.20.3229 as of Sep  4 2018
       Type HELP; for help and EXIT; to exit.
       Connected to OpenLink Virtuoso
       Driver: 07.20.3229 OpenLink Virtuoso ODBC Driver
       SQL>

3. Puis on pointe vers le(s) fichier(s) en définissant le nom du graphe et on lance le loader:

       SQL> ld_dir ('/home/bilal/git/virtuoso/share/virtuoso/vad', 'wheat.ttl.gz', 'http://latest.knetminer.wheat.ac.uk');
       Done. -- 1 msec.

       SQL> rdf_loader_run();
       Done. -- 198048 msec.

**Note**:
Il vaut mieux utiliser la ligne de commande plutôt que l'interface graphique.

Pour plus de détails :
* [Loading Data Virtuoso](https://github.com/dbpedia/dbpedia-docs/wiki/Loading-Data-Virtuoso).
* [Bulk Loading RDF Source Files into one or more Graph IRIs](http://vos.openlinksw.com/owiki/wiki/VOS/VirtBulkRDFLoader).

#### Exécuter le script:

En tapant:

    $ python3 knetminer_extraction_genes.py
    Please choose a DB (Enter a number):
    1.Wheat
    2.Arabidopsis

Puis on choisit la base (ex. 1)

Le script:

1. Fait l'extraction.
2. Génère le CSV pour chaque espèce.
3. Convertit ces derniers en JSONs, en utilisant le script `csv2json_knetminer.jq`.
4. Compresse les CSV et les JSON.

#### Output

**Exemple d'output:**

```
output_wheat
├── knet_triticum_aestivum_20190701.csv.gz
├── knet_triticum_aestivum_20190701.json.gz
├── knet_arabidopsis_thaliana_20190701.csv.gz
├── knet_arabidopsis_thaliana_20190701.json.gz
├── taxons_names.json
└── wheat_genes_uri.json
```

* `knet_triticum_aestivum_20190701.csv.gz`: le fichier CSV compressé contenant les informations sur les gènes extraits.
* `knet_triticum_aestivum_20190701.json.gz`: le JSON compressé issue de la transformation CSV2JSON
* `taxons_names.json`: La liste des noms des taxons extraits (utilisés pour le cache)
* `wheat_genes_uri.json`: La liste des URI des gènes extraits (utilisés pour le cache)

Ces fichiers de sortie sont stockés dans le répertoire `output_knetminer/output_wheat`.

En général, l'output aura comme nom: `knet_<specie>_<extraction_date>.<extension>`

**Exemple du contenu du fichier JSON:**
```
[
  {
    "entryType": "Gene annotation",
    "name": "araknet:A0A1P8APF4_AT1G07480",
    "databaseName": "KnetMiner",
    "description": "Gene ID A0A1P8APF4_AT1G07480 has alternative name(s): At1g07480, F22G5.14, F22G5_14, PTN002652815, Transcription factor IIA, alpha/beta subunit, UniProtKB:A0A1P8APF4, Start position 2296023 and End position 2299544 on Chromosome 1, The species is 'Arabidopsis thaliana' (https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=taxonomy&id=3702). It encodes protein(s): PROTEIN_AT1G07480_3, has prefered name: 'AT1G07480.3' PROTEIN_AT1G07480_1, has prefered name: 'AT1G07480.1' PROTEIN_AT1G07480_2, has prefered name: 'AT1G07480.2'. It participates in the following biological process(es): BIOPROC_GO_0006351_GO_0006351_GO_0006351_GO_0006351 has alternative name(s) 'DNA-dependent transcription,GO:0006351,cellular transcription, DNA-dependent,transcription, DNA-dependent' (biological process description : The cellular synthesis of RNA on a template of DNA.). BIOPROC_GO_0006366_GO_0006366_GO_0006366 has alternative name(s) 'GO:0006366,transcription from Pol II promoter,transcription from RNA polymerase II promoter' (biological process description : The synthesis of RNA from a DNA template by RNA polymerase II, originating at an RNA polymerase II promoter. Includes transcription of messenger RNA (mRNA) and certain small nuclear RNAs (snRNAs).).",
    "url": "http://knetminer.rothamsted.ac.uk/araknet/genepage?list=AT1G07480",
    "species": [
      "Arabidopsis thaliana"
    ],
    "node": "Rothamsted Research"
  }
  ...
]
```
